$(document).ready(function () {
    let select = '';
    let timeout_link;

    if ($('.js--select-styled').length) {
        $('.js--select-styled').styler({
                onSelectClosed: function () {
                    if ($(this).hasClass('js--type')) {
                        select = $(this).find('option:selected').data('val');
                        $('.inp-type').val($(this).find('option:selected').val());

                        if (select == "item" || select == "clothes" || select == "footwear") {
                            $('.select__block').eq(0).addClass('active').siblings().removeClass('active');

                           $('.inp-hiend').prop('disabled','disabled');
                           $('.inp-angle').removeAttr('disabled');

                        } else {
                            if (select == "food") {
                                $('.select__block').eq(1).addClass('active').siblings().removeClass('active');

                                $('.inp-hiend').prop('disabled','disabled');
                                $('.inp-angle').prop('disabled','disabled');
                            } else {
                                if (select == "jeweler") {
                                    $('.select__block').eq(2).addClass('active').siblings().removeClass('active');

                                    $('.inp-hiend').removeAttr('disabled');
                                    $('.inp-angle').prop('disabled','disabled');
                                } else {
                                    if (select == "promotion") {
                                        $('.select__block').eq(3).addClass('active').siblings().removeClass('active');

                                        $('.inp-hiend').prop('disabled','disabled');
                                        $('.inp-angle').prop('disabled','disabled');
                                    }
                                }
                            }
                        }
                    }

                    if ($(this).hasClass('js--angle')) {
                        $('.inp-angle').val($('.js--angle').find('option:selected').val());
                    }

                    count_price();
                }
            }
        )
    }

    $('.js--hiend').on('change', function () {
        count_price();

        if ($('.js--hiend').is(':checked')) {
            $('.inp-hiend').val('Нужна Сложная HI-END обработка ')
        } else {
            $('.inp-hiend').val('');
        }
    });

    $(document).on('keydown', '.js--number', function (e) {
        input_number();
    });

// ввод количества с клавиатуры
    $(document).on('input', '.js--number', function () {

        if ($(this).data("lastval") != $(this).val()) {
            if ($(this).val() == '') {
                $(this).prop('value', $(this).data("lastval"))
            } else {
                var value = $(this).prop('value');
                value = Number(value);
                if (isNaN(value)) {
                    $(this).prop('value', $(this).data("lastval"))
                } else {
                    $(this).prop('value', value);
                }
            }
            ;
            $(this).data("lastval", $(this).val());

            if (timeout_link) {
                clearTimeout(timeout_link)
            }
            timeout_link = setTimeout(function () {
                count_price();
                $('.inp-amount').val($('.js--amount').val());
            }, 250)

        }
        ;
    });


    var input_number = function () {
        var allow_meta_keys = [86, 67, 65];
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 ||
            ($.inArray(event.keyCode, allow_meta_keys) > -1 && (event.ctrlKey === true || event.metaKey === true)) ||
            (event.keyCode >= 35 && event.keyCode <= 39)) {
            return;
        } else {
            if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
            }
        }
    };

    function count_price() {
        let price = 0;
        let item_price = 0;
        let angle_add = 0;

        if (select == "item" || select == "clothes" || select == "footwear") {
            let angle = Number($('.js--angle').find('option:selected').val());
            if (!isNaN(angle)) {
                if (angle > 3) {
                    angle_add = (angle - 3) * 50;
                }
            }

            let amount = Number($('.js--amount').val());

            if (amount <= 20) {
                item_price = 500
            } else {
                if (amount <= 50) {
                    item_price = 450
                } else {
                    if (amount <= 100) {
                        item_price = 400
                    } else {
                        item_price = 350
                    }
                }
            }
            ;

            price = amount * (item_price + angle_add);

        } else {
            if (select == "food") {
                let amount = Number($('.js--amount').val());
                price = amount * 350;

                if (price < 6000) {
                    price = 6000;
                }

            } else {
                if (select == "jeweler") {
                    let check_add = 0;
                    if ($('.js--hiend').is(':checked')) {
                        check_add = 1000;
                    }

                    let amount = Number($('.js--amount').val());
                    price = amount * (1500 + check_add);

                } else {
                    if (select == "promotion") {
                        let amount = Number($('.js--amount').val());
                        price = amount * 2000;

                        if (price < 6000) {
                            price = 6000;
                        }
                    }
                }
            }
        }

        $('.calculate-total').text(price + " руб.")
    }

    if ($('.service__slider__el').length > 1) {
        $('.service__slider').owlCarousel({
            margin: 5,
            loop: true,
            nav: true,
            dots: false,
            autoplay: false,
            navText: [,],
            autoplay: true,
            autoplayHoverPause: false,
            autoplayTimeout: 3000,
            smartSpeed: 1000,
            items: 1,
            onInitialized: function (event) {
                $('.service__slider .owl-prev').html('<svg viewBox="0 -256 1792 1792">' +
                    '<g transform="matrix(1,0,0,-1,98.711864,1224.678)" ><path d="M 1536,640 V 512 q 0,-53 -32.5,-90.5 Q 1471,384 1419,384 H 715 L 1008,90 q 38,-36 38,-90 0,-54 -38,-90 l -75,-76 q -37,-37 -90,-37 -52,0 -91,37 L 101,486 q -37,37 -37,90 0,52 37,91 l 651,650 q 38,38 91,38 52,0 90,-38 l 75,-74 q 38,-38 38,-91 0,-53 -38,-91 L 715,768 h 704 q 52,0 84.5,-37.5 Q 1536,693 1536,640 z"       /></g>' +
                    '</svg>')
                $('.service__slider .owl-next').html('<svg viewBox="0 0 1000 1000">' +
                    '<g ><path d="M963,500c0,22.6-7.8,41.7-23.3,57.2L530.2,966.7C513.8,982.2,494.8,990,473,990c-21.4,0-40.3-7.8-56.6-23.3l-47.2-47.2c-15.9-15.9-23.9-35-23.9-57.2c0-22.2,8-41.3,23.9-57.2l184.3-184.3H110.6c-21.8,0-39.5-7.9-53.2-23.6C43.9,581.5,37,562.5,37,540.3v-80.5c0-22.2,6.8-41.2,20.4-56.9c13.6-15.7,31.3-23.6,53.2-23.6h442.8L369.2,194.3c-15.9-15.1-23.9-34-23.9-56.6c0-22.6,8-41.5,23.9-56.6l47.2-47.2C432.3,18,451.1,10,473,10c22.2,0,41.3,8,57.2,23.9l409.5,409.5C955.2,458.1,963,476.9,963,500z"       /></g>' +
                    '</svg>')
            }
        })
    }

    if ($('.client-slider__el').length > 1) {
        let client_counter = $('.client-slider__el').length;
        $('.client-counter__total').text('/ ' + client_counter);

        var owl = $('.client-slider');
        owl.owlCarousel({
            margin: 0,
            loop: true,
            nav: true,
            dots: true,
            autoplay: false,
            navText: [,],
            autoplayHoverPause: false,
            autoplayTimeout: 5000,
            smartSpeed: 1000,
            items: 1,
            onInitialized: function (event) {
                $('.client-slider .owl-prev').html('<svg viewBox="0 -256 1792 1792">' +
                    '<g transform="matrix(1,0,0,-1,98.711864,1224.678)" ><path d="M 1536,640 V 512 q 0,-53 -32.5,-90.5 Q 1471,384 1419,384 H 715 L 1008,90 q 38,-36 38,-90 0,-54 -38,-90 l -75,-76 q -37,-37 -90,-37 -52,0 -91,37 L 101,486 q -37,37 -37,90 0,52 37,91 l 651,650 q 38,38 91,38 52,0 90,-38 l 75,-74 q 38,-38 38,-91 0,-53 -38,-91 L 715,768 h 704 q 52,0 84.5,-37.5 Q 1536,693 1536,640 z"       /></g>' +
                    '</svg>')
                $('.client-slider .owl-next').html('<svg viewBox="0 0 1000 1000">' +
                    '<g ><path d="M963,500c0,22.6-7.8,41.7-23.3,57.2L530.2,966.7C513.8,982.2,494.8,990,473,990c-21.4,0-40.3-7.8-56.6-23.3l-47.2-47.2c-15.9-15.9-23.9-35-23.9-57.2c0-22.2,8-41.3,23.9-57.2l184.3-184.3H110.6c-21.8,0-39.5-7.9-53.2-23.6C43.9,581.5,37,562.5,37,540.3v-80.5c0-22.2,6.8-41.2,20.4-56.9c13.6-15.7,31.3-23.6,53.2-23.6h442.8L369.2,194.3c-15.9-15.1-23.9-34-23.9-56.6c0-22.6,8-41.5,23.9-56.6l47.2-47.2C432.3,18,451.1,10,473,10c22.2,0,41.3,8,57.2,23.9l409.5,409.5C955.2,458.1,963,476.9,963,500z"       /></g>' +
                    '</svg>')
            },
            responsive: {
                0: {
                    items: 2
                },
                750: {
                    items: 3
                },
                1279: {
                    items: 5
                }
            }
        });

        var subtrahend = 4;

        if ($(window).width() < 1280) {
            subtrahend = 2
        }
        if ($(window).width() < 1023) {
            subtrahend = 1
        }

        owl.on('changed.owl.carousel', function (event) {
            $('.client-counter__current').text(event.item.index - subtrahend)
        })
    }

    var mobile_loading = false;

    $(document).on('click', '.js--load-photo', function () {
        $('.portfolio-grid .mobile-el').removeClass('mobile-el');
        $(this).closest('.text-center').remove();

        return false;
    });

    $(document).on('click', '.js--toggle-photo', function () {
        $('.portfolio-grid__el').stop().slideToggle(100);
        if ($('.portfolio-grid__el').is('hidden')) {
            $(this).find('strong').text('Показать фото')
        } else {
            $(this).find('strong').text('Скрыть фото')
        }

        return false;
    });

    $('.js--change-title').on('click', function () {
        $('.callback-form h2').text($(this).data('title'))
    });

    if ($('#map').length) {
        ymaps.ready(function () {
            var point = $('.map').data('img');

            var myMap = new ymaps.Map("map", {
                center: [55.762227, 37.553307],
                zoom: 16,
                controls: ["zoomControl"]
            });

            myPlacemark = new ymaps.Placemark(myMap.getCenter(), {}, {});

            myMap.geoObjects.add(myPlacemark)
            myMap.behaviors.disable("scrollZoom");
        });
    }

    $(document).on('click', '.js--page-scroll', function () {
        $("html, body").animate({
            scrollTop: $($(this).attr("href")).offset().top
        }, {
            duration: 500
        });
        return false;
    });

    $('.js--toggle-menu').on('click', function () {
        $('.header').toggleClass('open');

        return false;
    });

    $('.js--add-data').on('click', function () {
        $('.inp-type').val($('.js--type option:selected').val());
        $('.inp-amount').val($('.js--amount option:selected').val());
        $('.inp-angle').val($('.js--angle option:selected').val());
    })

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    $('.inp-phone').mask('+7(999)999-99-99');

    $(document).on('click', '.js--form-submit', function () {

        var btn = $(this);
        var form = btn.closest('.main-form');
        var errors = false;

        $(form).find('.required').each(function () {
            var inp = $(this);
            var val = inp.prop('value');
            if (val == '') {
                inp.addClass('error');
                errors = true;
            } else {
                if (inp.hasClass('inp-mail')) {
                    if (validateEmail(val) == false) {
                        inp.addClass('error');
                        errors = true;
                    }
                }
            }
        });

        if (errors == false) {
            var button_value = btn.html();
            btn.text('Отправляем...');

            var method = form.attr('method');
            var data = form.serialize();
            var action = form.attr('action');

            $.ajax({
                type: method,
                url: action,
                data: data,
                success: function (data) {
                    form.find('.inp').each(function () {
                        $(this).prop('value', '')
                    });
                    btn.html(button_value);
                    $.fancybox.open({
                        src: '#thanks',
                        type: 'inline'
                    });
                },
                error: function (data) {
                    btn.html('Ошибка');
                    setTimeout(function () {
                        btn.text(button_value);
                    }, 2000);
                }
            });
        }

        return false;
    });

    $('.inp').focus(function () {
        $(this).removeClass('error');
        $('body').toggleClass('fixed');
    })

})
;
