<?php
    $sendto = "zemotziv@yandex.ru";
    $type = nl2br($_POST['type']);
    $amount = nl2br($_POST['amount']);
    $angle = nl2br($_POST['angle']);
    $hiend = nl2br($_POST['hiend']);
    $name = nl2br($_POST['name']);
    $phone = nl2br($_POST['phone']);

    $content = "Заявка с сайта ПРЕДМЕТФОТО";
    // Формирование заголовка письма
    $subject  = $content;
    $headers  = "From: no-replay@no-replay.ru" . "\r\n";
    $headers .= "Reply-To: Без ответа". "\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: text/html;charset=utf-8 \r\n";

    // Формирование тела письма
    $msg  = "<html><body style='font-family:Arial,sans-serif;'5>";
    $msg .= "<h2 style='font-weight:bold;border-bottom:1px dotted #ccc;'>Письмо с сайта ПРЕДМЕТФОТО</h2>\r\n";
    $msg .= "<p><strong>Имя:</strong> ".$name."</p>\r\n";
    $msg .= "<p><strong>Телефон:</strong> ".$phone."</p>\r\n";
    if ($type) $msg .= "<p><strong>Тип съемки:</strong> ".$type."</p>\r\n";
    if ($amount) $msg .= "<p><strong>Количество товара:</strong> ".$amount."</p>\r\n";
    if ($angle) $msg .= "<p><strong>Количество ракурсов:</strong> ".$angle."</p>\r\n";
    if ($hiend) $msg .= "<p><strong>Дополнительно:</strong> ".$hiend."</p>\r\n";

    $msg .= "</body></html>";

    // отправка сообщения
    if(@mail($sendto, $subject, $msg, $headers)) {
        echo "true";
    } else {
        echo "false";
    }

?>